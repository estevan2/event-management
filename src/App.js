import Header from "./components/Header";
import Routes from "./routes";
import { GlobalStyles } from "./style/global";


function App() {
  return (
    <>
      <Header />
      <GlobalStyles />
      <Routes />
    </>
  )
}

export default App;
