import axios from "axios"
import { createContext, useEffect, useState } from "react"

export const BeersContext = createContext([])

export const BeersProvider = ({ children }) => {
    const [beers, setBeers] = useState([])

    useEffect(() => {
        axios
            .get(`https://api.punkapi.com/v2/beers?per_page=80`)
            .then(res => {
                setBeers(res.data)
            })
            .catch(err => console.log(err))
    }, [])

    return (
        <BeersContext.Provider value={{ beers }}>
            {children}
        </BeersContext.Provider>
    )
}