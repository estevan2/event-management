import { createContext, useState } from "react"

export const BeersConfContext = createContext([])

export const BeersConfProvider = ({ children }) => {
    const [beersConf, setBeersConf] = useState([])

    const addBeersConf = beer => {
        setBeersConf([...beersConf, beer])
    }

    const removeBeersConf = beer => {
        const newList = beersConf.filter(item => beer.name !== item.name)
        setBeersConf(newList)
    }

    return (
        <BeersConfContext.Provider value={{ beersConf, addBeersConf, removeBeersConf }}>
            {children}
        </BeersConfContext.Provider>
    )
}