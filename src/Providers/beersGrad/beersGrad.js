import { createContext, useState } from "react"

export const BeersGradContext = createContext([])

export const BeersGradProvider = ({ children }) => {
    const [beersGrad, setBeersGrad] = useState([])

    const addBeersGrad = beer => {
        setBeersGrad([...beersGrad, beer])
    }

    const removeBeersGrad = beer => {
        const newList = beersGrad.filter(item => beer.name !== item.name)
        setBeersGrad(newList)
    }

    return (
        <BeersGradContext.Provider value={{ beersGrad, addBeersGrad, removeBeersGrad }}>
            {children}
        </BeersGradContext.Provider>
    )
}