import { createContext, useState } from "react"

export const BeersMarContext = createContext([])

export const BeersMarProvider = ({ children }) => {
    const [beersMar, setBeersMar] = useState([])

    const addBeersMar = beer => {
        setBeersMar([...beersMar, beer])
    }

    const removeBeersMar = beer => {
        const newList = beersMar.filter(item => beer.name !== item.name)
        setBeersMar(newList)
    }

    return (
        <BeersMarContext.Provider value={{ beersMar, addBeersMar, removeBeersMar }}>
            {children}
        </BeersMarContext.Provider>
    )
}