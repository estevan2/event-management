import { BeersProvider } from "./beers/beers";
import { BeersConfProvider } from "./beersConf/beersConf";
import { BeersGradProvider } from "./beersGrad/beersGrad";
import { BeersMarProvider } from "./beersMar/beersMar";

const Providers = ({ children }) => {

    return (
        <BeersProvider>
            <BeersConfProvider>
                <BeersGradProvider>
                    <BeersMarProvider>
                        {children}
                    </BeersMarProvider>
                </BeersGradProvider>
            </BeersConfProvider>
        </BeersProvider>
    )
}

export default Providers