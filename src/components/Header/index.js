import { AppBar, makeStyles, Toolbar, Typography } from "@material-ui/core"
import { Link } from "react-router-dom"

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    link: {
        color: "#FFF",
        textDecoration: "none",
        margin: "10px",
    },
    toolbar: {
        display: "flex",
        justifyContent: "space-between",
    },
}))

const Header = () => {

    const classes = useStyles()

    return (
        <AppBar>
            <Toolbar className={classes.toolbar}>
                <Link to="/" className={classes.link}>
                    <Typography variant="h5">
                        Event Management
                    </Typography>
                </Link>
                <nav className={classes.toolbar}>
                    <Link to="/graduation" className={classes.link}>
                        <Typography>
                            Graduation
                        </Typography>
                    </Link>
                    <Link to="/marriage" className={classes.link}>
                        <Typography>
                            Marriage
                        </Typography>
                    </Link>
                    <Link to="/confraternization" className={classes.link}>
                        <Typography>
                            Confraternization
                        </Typography>
                    </Link>
                </nav>
            </Toolbar>
        </AppBar>
    )
}

export default Header