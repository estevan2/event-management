import { Button, Card, makeStyles } from "@material-ui/core"
import { useContext } from "react"
import { BeersConfContext } from "../../Providers/beersConf/beersConf"

const useStyles = makeStyles({
    root: {
        marginTop: 80,
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
    },
    card: {
        width: 200,
        margin: 10,
        marginTop: 30,
        padding: 10,
        boxSizing: "border-box",
        textAlign: "center",
    },
    title: {
        margin: "0",
        height: 44,
    },
    figure: {
        height: 150,
    },
    img: {
        maxHeight: "100%",
    },
    button: {
        boxSizing: "border-box",
        width: 180,
        marginTop: 10,
    },
})

const Confraternization = () => {

    const classes = useStyles()
    const { beersConf, removeBeersConf } = useContext(BeersConfContext)

    return (
        <div className={classes.root}>
            {beersConf.length > 0 ? (beersConf.map(item => (
                <Card className={classes.card} key={item.id}>
                    <h3 className={classes.title}>{item.name}</h3>
                    <figure className={classes.figure}>
                        <img className={classes.img} src={item.image_url} alt={item.name} />
                    </figure>
                    <p>First Brewed: {item.first_brewed}</p>
                    {/* <p>Description: {item.description}</p> */}
                    <p>Amount: {item.volume.value} {item.volume.unit}</p>
                    <Button
                        className={classes.button}
                        variant="contained"
                        color="secondary"
                        onClick={() => removeBeersConf(item)}
                    >
                        Remove
                    </Button>
                </Card>)
            )) : (<h3>Empty</h3>)}
        </div>
    )
}

export default Confraternization