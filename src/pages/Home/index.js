import { Button, Card, makeStyles } from "@material-ui/core"
import { useContext } from "react"
import { BeersContext } from "../../Providers/beers/beers"
import { BeersConfContext } from "../../Providers/beersConf/beersConf"
import { BeersGradContext } from "../../Providers/beersGrad/beersGrad"
import { BeersMarContext } from "../../Providers/beersMar/beersMar"

const useStyles = makeStyles({
    root: {
        marginTop: 80,
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
    },
    card: {
        width: 200,
        margin: 10,
        marginTop: 30,
        padding: 10,
        boxSizing: "border-box",
        textAlign: "center",
    },
    title: {
        margin: "0",
        height: 44,
    },
    figure: {
        height: 150,
    },
    img: {
        maxHeight: "100%",
    },
    button: {
        boxSizing: "border-box",
        width: 180,
        marginTop: 10,
    },
})

const Home = () => {
    const { beers } = useContext(BeersContext)
    const { addBeersGrad } = useContext(BeersGradContext)
    const { addBeersMar } = useContext(BeersMarContext)
    const { addBeersConf } = useContext(BeersConfContext)
    const classes = useStyles()

    return (
        <div className={classes.root}>
            {beers.map(item => (
                <Card className={classes.card} key={item.id}>
                    <h3 className={classes.title}>{item.name}</h3>
                    <figure className={classes.figure}>
                        <img className={classes.img} src={item.image_url} alt={item.name} />
                    </figure>
                    <p>First Brewed: {item.first_brewed}</p>
                    {/* <p>Description: {item.description}</p> */}
                    <p>Amount: {item.volume.value} {item.volume.unit}</p>
                    <Button
                        className={classes.button}
                        variant="contained"
                        color="primary"
                        onClick={() => addBeersGrad(item)}
                    >
                        Add to Graduation
                    </Button>
                    <Button
                        className={classes.button}
                        variant="contained"
                        color="primary"
                        onClick={() => addBeersMar(item)}
                    >
                        Add to Marriage
                    </Button>
                    <Button
                        className={classes.button}
                        variant="contained"
                        color="primary"
                        onClick={() => addBeersConf(item)}
                    >
                        Add to Confraternization
                    </Button>
                </Card>
            ))}
        </div>
    )
}

export default Home