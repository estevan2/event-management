import { Route, Switch } from "react-router-dom"
import Confraternization from "../pages/Confraternization"
import Graduation from "../pages/Graduation"
import Home from "../pages/Home"
import Marriage from "../pages/Marriage"

const Routes = () => {

    return (
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route path="/graduation">
                <Graduation />
            </Route>
            <Route path="/marriage">
                <Marriage />
            </Route>
            <Route path="/confraternization">
                <Confraternization />
            </Route>
        </Switch>
    )
}

export default Routes